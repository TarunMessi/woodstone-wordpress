<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Woodstone' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' ); 

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{Y-Ekr,2C|xEi6/`x%2/a2)^5T.t<]G3GuS^[qGhT)Jm N]ACNMKh56+8WsX/n;4' );
define( 'SECURE_AUTH_KEY',  'p4*3(XzF;%T:;ZOvEqd&U(+nyAEnx#_K?E*{0k~ClJ^[LTge#{3r B4y,G)^~a4/' );
define( 'LOGGED_IN_KEY',    '#56IALQLwJoi,L@@sROf9R;UvLXh($ZqlGC@F+`wrGO58N:,hb]A~B{q(e-L(?(x' );
define( 'NONCE_KEY',        'D=aJ,v8H{ vG($:4M! pm@78q`AyEM?C30Kjhu HE=Dnn!uEaHwlSUL?c&&=O~CK' );
define( 'AUTH_SALT',        'Be;caw_8a<utdlm77wge*GAoE3u_fQzh6)#ILABU2_IL~K_(mWoin}k^ 269!z7>' );
define( 'SECURE_AUTH_SALT', '?!QF743sriPDR#>:M4eO#B)B`-R{u2$yjJ{,aLM_7zHpT7S_+/A7k2S_b[bYfmNn' );
define( 'LOGGED_IN_SALT',   'xkf+^#iu)JB#lZxrW,O0;fteSFeeT|Z0p1Pq2gYb.wbm>P$?i$Q0 ^cY-vSXt)<V' );
define( 'NONCE_SALT',       '<O_rqF?/z;,BM&yOzCo(|<KU*yaz[Q@WkJp.PJlZ.t39#P aWDEniaS:E&Om!#2m' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

define( 'FS_METHOD', 'direct' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

/** Removes Events from WP Admin Bar  */
define('TRIBE_DISABLE_TOOLBAR_ITEMS', true);
