<?php
/**
 * Plugin Name: Moko Notifications
 * Plugin URI: http://mokoapp.com
 * Description: A simple plugin to send push notifications through MokoApp
 * Author: Rajesh Kumar at Moko
 * Author URI: mailto:support@mokoapp.com
 * Version: 0.3
 * License: GPL2+
 */

add_action('admin_menu', 'pns_setup_menu');

function pns_setup_menu(){
  $user = wp_get_current_user();
    if ( in_array( 'woodstone_admin', (array) $user->roles ) ) {
      add_menu_page( 'Moko Notifications', 'Moko Notifications', 'woodstone_admin', 'moko-notifications', 'pns_init', 'dashicons-flag' );
    }else{
    add_menu_page( 'Moko Notifications', 'Moko Notifications', 'administrator', 'moko-notifications', 'pns_init', 'dashicons-flag' );
  }
}

add_action('admin_enqueue_scripts', 'pns_load_scripts');

function pns_load_scripts() {
	wp_enqueue_script( 'mokoapp-notifications', plugins_url( '/js/mokoapp-notifications.js', __FILE__ ) );
}

function pns_init(){
    $html  = '<h1>Moko Notifications</h1>';
    $html .= '<p style="font-style:italic;">Use this form to send push notifications to mobile apps</p>';
    $html .= '<label for="notification_message" style="font-size:16px;">Message</label><br/>';
    $html .= '<textarea id="notification_message" maxlength="240" style="margin-top:10px;min-width:400px;min-height:100px;"></textarea><br/>';
    $html .= '<label id="msg_chars_left"><span>240</span> character(s) available</label><br/>';

        $html .= '<label style="font-size:16px;padding-top:10px;padding-bottom:10px;display:block;">Tags for Notification</label>';
        // $html .= '<label style="padding:5px 0;display:block;">* if no tags are selected, all users will receive notification</label>';

        $tags = ['Pool Alerts' =>'pool_alerts','Emergency Alerts' => 'emergency_alerts','Tot Lots' => 'tot_lots','Trash & Recycling' => 'trash_recycling','Association Events' => 'association_events','Snow Removal' => 'snow_removal','Parking' => 'parking','General' => 'general','Community Events' => 'community_events','Committee/Board Meetings' => 'board_meetings'];

        /* Notification Tags */
        foreach ( $tags as $tagKey => $tagVal ) {

          $html .= '<input type="checkbox" name="notification_tag_' . $tagVal . '" class="notification-tag" value="0" data-tag="' .  $tagVal . '"></input>';
          $html .= '<label for="notification_tag_' . $tagVal . '">' . $tagKey . '</label><br/>';
        }

        $html .= '<div><div class="notification-scheduler" style="padding:10px;background:white;margin-top:20px;display:inline-block;"><div class="field" id="push_now"><input checked id="push_now_flag" type="checkbox"><label for="push_now_flag">Deliver Message Immediately</label></div><div class="field" id="push_send_at" style="opacity:0.3"><label for="notification_send_at">Send at</label><select disabled="disabled" id="notification_send_at_1i" name="notification[send_at(1i)]"><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option><option value="2026">2026</option><option value="2027">2027</option><option value="2028">2028</option><option value="2029">2029</option><option value="2030">2030</option></select><select disabled="disabled" id="notification_send_at_2i" name="notification[send_at(2i)]"><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select><select disabled="disabled" id="notification_send_at_3i" name="notification[send_at(3i)]"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>&nbsp;&mdash;&nbsp;<select disabled="disabled" id="notification_send_at_4i" name="notification[send_at(4i)]"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>&nbsp;:&nbsp;<select disabled="disabled" id="notification_send_at_5i" name="notification[send_at(5i)]"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select></div></div></div>';

    $html .= '<button id="push_notifier_btn" class="button button-primary" style="margin-top:15px;">Send Notification</button><br/>';
    $html .= '<label id="notification_notice" style="position:relative;top:10px;font-size:14px;"></label>';
    echo $html;
}

?>
